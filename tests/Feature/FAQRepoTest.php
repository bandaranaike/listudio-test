<?php

namespace Tests\Feature;

use App\Model\FAQ;
use App\Repositories\FAQRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FAQRepoTest extends TestCase
{
    /**
     * @var FAQRepository
     */
    private $repo;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->repo = new FAQRepository();
    }

    public function testCreate()
    {
        $result = $this->repo->create('What is the laravel ?');
        self::assertInstanceOf(FAQ::class, $result);
    }

    /**
     *
     */
    public function testChangePublishStatus()
    {
        $item = $this->repo->create('What is flutter?');
        $item = $this->repo->getById($item->id);
        $this->repo->togglePublishStatus($item->id);
        $item_again = $this->repo->getById($item->id);;
        self::assertEquals($item->publish_status, !$item_again->publish_status);
    }

    public function testDelete()
    {
        $res      = $result = $this->repo->create('What is TDD ?');
        $response = $this->repo->delete($res->id);
        self::assertTrue($response);
    }


    public function testList()
    {

        $items1 = $this->repo->getAllList();
        $this->repo->create('What is VueJS ?');
        $items2 = $this->repo->getAllList();

        self::assertInstanceOf(Collection::class, $items1);
        self::assertEquals($items1->count() + 1, $items2->count());
    }

    public function testGetPublishedList()
    {
        $items = $this->repo->getPublishedList();
        self::assertInstanceOf(Collection::class, $items);
    }

    public function testUpdateQuestion()
    {
        $updating_value = 'What is VueJs?';
        $item           = $this->repo->create('What is Vue?');
        $updated_item   = $this->repo->updateQuestion($item->id, $updating_value);

        self::assertEquals($updating_value, $updated_item->question);
    }

}
