@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">
            User FAQs
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <td>Question</td>
                    <td>Answer</td>
                    <td>User</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>
                @foreach($faqs as $faq)
                    <tr id="data-track-{{$faq->id}}">
                        <td>{{$faq->question}}</td>
                        <td class="answer">{{$faq->answer}}</td>
                        <td>{{$faq->user? $faq->user->name : ' - '}}</td>
                        <td>
                            <div class="btn-group-xs" data-id="{{$faq->id}}">
                                @if($faq->publish_status)
                                    <a class="btn btn-info make-publish"> Published </a>
                                @else
                                    <a class="btn btn-default make-publish"> Make published </a>
                                @endif
                                <a class="btn btn-default">Delete</a>
                                <a class="btn btn-default">Edit</a>
                                <a class="btn btn-default" data-toggle="modal" data-item="{{json_encode($faq)}}" data-target="#add_answer">Add answer</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="add_answer" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add an answer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="question"></p>
                    <textarea id="answer" class="form-control"></textarea>
                    <div id="notify_user" class="d-none mt-3">
                        <input type="checkbox"> Notify user
                    </div>
                    <input type="hidden" id="item_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="save_answer">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent

    <script>
        $(function () {
                $('.make-publish').click(function (item) {
                    let button = $(item.currentTarget);
                    let id = button.parent().data('id');
                    $.ajax({
                        url: '/admin/faq/toggle-publish-status',
                        type: 'PATCH',
                        data: {id},
                        error: e => {
                            console.error(e);
                        },
                        success: r => {
                            if (r) {
                                button.text('Published').addClass('btn-info').removeClass('btn-default')
                            } else {
                                button.text('Make published').addClass('btn-default').removeClass('btn-info')
                            }
                        }
                    });
                });
            }
        );

        $('#add_answer').on('shown.bs.modal', function (e) {
            let item = $(e.relatedTarget).data('item');
            $('#question').text(item.question);
            $('#answer').val(item.answer);
            if (item.user)
                $('#notify_user').removeClass('d-none');
            $('#item_id').val(item.id)
        });

        $('#save_answer').click(() => {
            $.ajax({
                type: 'put',
                url: '/admin/faq/add-answer',
                data: {
                    answer: $('#answer').val(),
                    notify_user: $('#notify_user input').val() ? 1 : 0,
                    id: $('#item_id').val()
                },
                success: r => {
                    $('#data-track-' + r.id).children('.answer').text(r.answer);
                    $('#add_answer').modal('hide');
                },
                error: e => {
                    console.error(e)
                }
            })
        })


    </script>

@endsection
