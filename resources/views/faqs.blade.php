@extends('layouts.app')

@section('content')
    <div class="content">
        <h1>FAQ</h1>
        <div class="row">
            <div class="col-lg-12">
                @foreach($faqs as $faq)
                    <h3>{{$faq->question}}</h3>
                    <p>{{$faq->answer}}</p>
                @endforeach
            </div>
        </div>
        <div class="my-4">
            @auth
                <a href="{{url('/admin/home')}}">Dashboard</a>
            @endauth
            @guest
                <a href="{{url('/login')}}">Dashboard</a>
            @endguest
        </div>
    </div>



    <div class="card">
        <div class="card-header">
            Ask your question
        </div>

        <div class="card-body">
            <form action="{{ route("faq.create") }}" method="POST">
                @csrf
                <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                    <label for="question">Question*</label>
                    <input type="text" id="question" name="question" class="form-control" value="{{ old('question') }}" required>
                    @if($errors->has('question'))
                        <em class="invalid-feedback">
                            {{ $errors->first('question') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.permission.fields.title_helper') }}
                    </p>
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                </div>
            </form>


        </div>
    </div>

    @if(\Illuminate\Support\Facades\Session::has('success'))
        <div class="text-success mb-4">{{\Illuminate\Support\Facades\Session::get('success')}}</div>
    @endif
@endsection

@section('scripts')
    @parent

@endsection
