<?php

namespace App\Providers;

use App\Repositories\Contracts\FAQRepositoryInterface;
use App\Repositories\FAQRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(FAQRepositoryInterface::class, FAQRepository::class);
    }
}
