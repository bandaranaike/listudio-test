<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function showAdminList()
    {
        return view('admin.menu.index');
    }
}
