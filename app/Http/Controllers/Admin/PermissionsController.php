<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePermissionsRequest;
use App\Http\Requests\Admin\UpdatePermissionsRequest;

class PermissionsController extends Controller
{
    /**
     * Display a listing of Permission.
     *
     * @return Factory|View|Void
     */
    public function index()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $permissions = Permission::all();

        return view('admin.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating new Permission.
     *
     * @return Response
     */
    public function create()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        return view('admin.permissions.create');
    }

    /**
     * Store a newly created Permission in storage.
     *
     * @param \App\Http\Requests\StorePermissionsRequest $request
     * @return Response
     */
    public function store(StorePermissionsRequest $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        Permission::create($request->all());

        Log::info('Permission created');

        return redirect()->route('admin.permissions.index');
    }


    /**
     * Show the form for editing Permission.
     *
     * @param Permission $permission
     * @return Factory|View|void
     */
    public function edit(Permission $permission)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.permissions.edit', compact('permission'));
    }

    /**
     * Update Permission in storage.
     *
     * @param UpdatePermissionsRequest $request
     * @param Permission $permission
     * @return RedirectResponse|void
     */
    public function update(UpdatePermissionsRequest $request, Permission $permission)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $permission->update($request->all());

        return redirect()->route('admin.permissions.index');
    }


    /**
     * Remove Permission from storage.
     *
     * @param Permission $permission
     * @return RedirectResponse|void
     * @throws Exception
     */
    public function destroy(Permission $permission)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $permission->delete();

        Log::info('Permission deleted');

        return redirect()->route('admin.permissions.index');
    }

    public function show(Permission $permission)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.permissions.show', compact('permission'));
    }

    /**
     * Delete all selected Permission at once.
     *
     * @param Request $request
     * @return Response
     */
    public function massDestroy(Request $request)
    {
        Permission::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }

}
