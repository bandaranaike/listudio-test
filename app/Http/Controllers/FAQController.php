<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddAnswerRequest;
use App\Http\Requests\ChangePublishStatusRequest;
use App\Model\FAQ;
use App\Repositories\Contracts\FAQRepositoryInterface;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class FAQController extends Controller
{

    /**
     * @var FAQRepositoryInterface
     */
    private $repository;

    public function __construct(FAQRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Factory|View
     */
    public function viewFAQ()
    {
        $faqs = $this->repository->getPublishedList();
        return view('faqs', compact('faqs'));
    }

    /**
     * @return Factory|View
     */
    public function showAdminList()
    {
        $faqs = $this->repository->getAllList();
        return view('admin.faq.index', compact('faqs'));
    }

    /**
     * @param ChangePublishStatusRequest $request
     * @return bool|JsonResponse
     */
    public function togglePublishStatus(ChangePublishStatusRequest $request)
    {
        try {
            $response = $this->repository->togglePublishStatus($request->input('id'));
            return new JsonResponse($response);
        } catch (NotFound $e) {
            return new JsonResponse($e->getMessage(), 404);
        }
    }

    /**
     * @param AddAnswerRequest $request
     * @return FAQ|JsonResponse
     */
    public function addAnswer(AddAnswerRequest $request)
    {
        try {

            $item = $this->repository->updateAnswer($request->input('id'), $request->input('answer'));
            Log::info('Answer updated:' . $item->id);

            // If user exists, Sending a notification to the user
            if ($request->has('notify_user') && $item->user) {
                $this->repository->notifyUser($item->user->id);
            }

            return $item;

        } catch (NotFound $e) {
            return new JsonResponse($e->getMessage(), 404);
        }
    }

    public function create(Request $request)
    {
        $this->repository->create($request->input('question'));
        return back()->with(['success' => 'Your question successfully saved.']);
    }
}
