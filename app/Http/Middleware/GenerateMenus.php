<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Lavary\Menu\Facade as Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('main_nav_bar', function ($menu) {
            $menu->add('Home');
            $menu->add('Our Services', 'our-services');
            $menu->add(' Discover More', ' discover-more');
            $menu->add(' About Us ', ' about-us');
            $menu->add('Contact Us', 'contact-us');
        });

        return $next($request);
    }
}
