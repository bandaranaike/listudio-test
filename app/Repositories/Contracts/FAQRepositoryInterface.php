<?php


namespace App\Repositories\Contracts;

use App\Model\FAQ;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Illuminate\Database\Eloquent\Collection;

interface FAQRepositoryInterface
{

    /**
     * @param $id
     * @return bool
     * @throws NotFound
     */
    public function togglePublishStatus($id): bool;

    /**
     * Create new record
     * @param string $question
     * @return FAQ
     */
    public function create(string $question): FAQ;

    /**
     * Delete unwanted records
     * @param int $id
     * @return bool
     * @throws NotFound
     */
    public function delete(int $id): bool;

    /**
     * @param int $id
     * @return FAQ
     * @throws NotFound
     */
    public function getById(int $id): FAQ;

    /**
     * @return Collection
     */
    public function getAllList(): Collection;

    /**
     * @return Collection
     */
    public function getPublishedList(): Collection;

    /**
     * @param int $user_id
     * @return bool
     */
    public function notifyUser(int $user_id): bool;

    /**
     * @param int $id
     * @param string $answer
     * @return FAQ
     * @throws NotFound
     */
    public function updateAnswer(int $id, string $answer): FAQ;

    /**
     * @param int $id
     * @param string $question
     * @return FAQ
     * @throws NotFound
     */
    public function updateQuestion(int $id, string $question): FAQ;

}
