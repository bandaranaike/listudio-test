<?php


namespace App\Repositories;


use App\Model\FAQ;
use App\Repositories\Contracts\FAQRepositoryInterface;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class FAQRepository implements FAQRepositoryInterface
{
    private $model;

    const PUBLISH_STATUS_PUBLISHED = true;

    public function __construct()
    {
        $this->setModel();
    }

    /**
     * @inheritDoc
     */
    public function togglePublishStatus($id): bool
    {
        $item                 = $this->getById($id);
        $item->publish_status = !$item->publish_status;
        $item->save();
        return $item->publish_status;
    }

    /**
     * @inheritDoc
     */
    public function create(string $question): FAQ
    {
        $this->model->question = $question;

        if (Auth::check())
            $this->model->user_id = Auth::id();

        $this->model->save();
        return $this->model;
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): bool
    {
        $item = $this->getById($id);
        return $item->delete();

    }

    /**
     * @inheritDoc
     */
    public function getById(int $id): FAQ
    {
        $item = $this->model->find($id);
        if ($item) {
            return $item;
        } else {
            throw new NotFound('Expected FAQ item does not exists');
        }
    }

    /**
     * @inheritDoc
     */
    public function getAllList(): Collection
    {
        return $this->model->all();
    }

    /**
     * @inheritDoc
     */
    public function getPublishedList(): Collection
    {
        return $this->model->with('user')->wherePublishStatus(self::PUBLISH_STATUS_PUBLISHED)->get();
    }

    /**
     * @inheritDoc
     */
    public function notifyUser(int $user_id): bool
    {
        // TODO: Implement notifyUser() method.
        return true;
    }

    /**
     * @inheritDoc
     */
    public function updateAnswer(int $id, string $answer): FAQ
    {
        return $this->update($id, 'answer', $answer);
    }

    /**
     * @param $id
     * @param $key
     * @param $value
     * @return FAQ
     * @throws NotFound
     */
    private function update($id, $key, $value)
    {
        $item       = $this->getById($id);
        $item->$key = $value;
        $item->save();
        return $item;
    }

    /**
     * @inheritDoc
     */
    public function updateQuestion(int $id, string $question): FAQ
    {
        return $this->update($id, 'question', $question);
    }

    /**
     * Set the model in one place.
     */
    private function setModel()
    {
        $this->model = new FAQ();
    }
}
